﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask3
{
    class Person //Object for storing person, made all public just for this exercise, better practice to use protected.
    {
        public string firstName { get ; set; }

        public string lastName { get; set; }

        public Person(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}
